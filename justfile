dc := "docker-compose"
exec_app := dc + " exec movies"

default_test := 'movies'

build:
    {{dc}} up -d --build

fixtures:
    {{ exec_app }} python manage.py shell -c "from django.contrib.auth import get_user_model; get_user_model().objects.create_superuser('dev@dev.com', 'dev')"
    {{ exec_app }} python manage.py fill_movie_db 5

test arg=default_test:
    {{ exec_app }} pytest -k {{arg}}

stop:
    {{dc}} down -v 

shell:
    {{ exec_app }} python manage.py shell

flush:
    {{ exec_app }} python manage.py flush
