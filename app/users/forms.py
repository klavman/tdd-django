from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import CustomerUser


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomerUser
        fields = ("email",)


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        model = CustomerUser
        fields = "__all__"
