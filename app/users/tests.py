from django.contrib.auth import get_user_model
from django.test import TestCase
from mimesis import Person

person = Person("es")


"""
El usuario por defecto usa username y password para autenticación.
En esta app users se va a heredar de las clases que proporciona Django para
extender el usuario y modificarlo. Va a usarse email en vez de username.
"""


class UsersManagersTests(TestCase):
    def test_create_user(self):
        email_ = person.email()
        password_ = person.password()
        User = get_user_model()
        user = User.objects.create(email=email_, password=password_)
        self.assertEqual(user.email, email_)
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertIsNone(user.username)  # AbstractUser username is None

        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")
        with self.assertRaises(ValueError):
            User.objects.create_user(email="", password=password_)

    def test_create_superuser(self):
        email_ = person.email()
        password_ = person.password()

        User = get_user_model()
        admin_user = User.objects.create_superuser(email=email_, password=password_)
        self.assertEqual(admin_user.email, email_)
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        self.assertIsNone(admin_user.username)

        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email=email_, password=password_, is_superuser=False
            )
