## Anotaciones

### user / pass
dev@dev.com:dev

- Se ha creado un modelo CustomerUser dónde se ha eliminado username y la autenticación es mediante email único.

- Se ha heredado de AbstractUser, modificando manager y forms.

## Docker basic

### Build image when change Dockerfile or Docker-compose
```console
docker-compose build 
```
### Up container from image (-d detach)
```console
docker-compose up -d 
```
### Down container with volumes
```console
docker-compose down -v Down container with volumes
```
### Postgres
```console
docker-compose exec movies python manage.py migrate --noinput
docker-compose exec movies-db psql --username=movies --dbname=movies_dev 
```
### Run test

```console
docker-compose exec movies pytest
```

## Pytest Commands

\# normal run
```console
$ docker-compose exec movies pytest
```

\# disable warnings
```console
$ docker-compose exec movies pytest -p no:warnings
```

\# run only the last failed tests
```console
$ docker-compose exec movies pytest --lf
```

\# run only the tests with names that match the string expression
```console
$ docker-compose exec movies pytest -k "movie and not all_movies"
```

\# stop the test session after the first failure
```console
$ docker-compose exec movies pytest -x
```

\# enter PDB after first failure then end the test session
```console
$ docker-compose exec movies pytest -x --pdb
```

\# stop the test run after two failures
```console
$ docker-compose exec movies pytest --maxfail=2
```

\# show local variables in tracebacks
```console
$ docker-compose exec movies pytest -l
```

\# list the 2 slowest tests
```console
$ docker-compose exec movies pytest  --durations=2
```

## Generate secret key
```console
openssl rand -base64 32
```

## Pytest Coverage
```console
docker-compose exec movies pytest -p no:warnings --cov=.
```

## Linting process 

> ignore exclude arguments, all config for tools is pyproject or setup.cfg

### Flake8
```console
docker-compose exec movies flake8 .
```
### Black

> La configuración de black y directorios ignorados está en pyproject.toml
> No ejecutar con --exclude

```console
docker-compose exec movies black --check .
docker-compose exec movies black .
```

### Isort

> La configuración de isort y directorios ignorados está en pyproject.toml

```console
docker-compose exec movies isort . --check-only
docker-compose exec movies isort . --diff
docker-compose exec movies isort .
```