from django.core.management.base import BaseCommand

from tests.movies.factory_movies import MovieFactory


class Command(BaseCommand):
    help = "Create random movies"

    def add_arguments(self, parser):
        parser.add_argument(
            "total",
            type=int,
            help="Indicates the number of movies to be created",
        )

    def handle(self, *args, **kwargs):
        total = kwargs["total"]
        MovieFactory.create_batch(total)
