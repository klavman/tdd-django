from django.db import models


class Movie(models.Model):
    """Model definition for Movie."""

    title = models.CharField(max_length=255)
    genre = models.CharField(max_length=255)
    year = models.CharField(max_length=4)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta definition for Movie."""

        verbose_name = "Movie"
        verbose_name_plural = "Movies"

    def __str__(self):
        """Unicode representation of Movie."""
        return f"{self.title} - {self.year}"
