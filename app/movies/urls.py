from django.urls import path

from .views import MovieDetail, MovieList

"""
Endpoint    HTTP Method    CRUD Method    Result
/api/movies     GET           READ        get all movies
/api/movies/:id GET           READ        get a single movie
/api/movies     POST          CREATE      add a movie
"""


urlpatterns = [
    path("api/movies/", MovieList.as_view()),
    path("api/movies/<int:pk>", MovieDetail.as_view(), name="api_single_movie"),
]
