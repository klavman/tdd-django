import factory

from mimesis_factory import MimesisField
from movies.models import Movie


class MovieFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Movie

    title = MimesisField("title")
    genre = MimesisField("word")
    year = MimesisField("year")
