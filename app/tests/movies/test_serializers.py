# import pytest

from movies.serializers import MovieSerializer
from .dummy_endpoint import dummy_movies_valid_data, dummy_movies_invalid_data


def test_valid_movie_serializer():
    valid_serializer_data = dummy_movies_valid_data.create(iterations=1)[0]
    serializer = MovieSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.data == valid_serializer_data
    assert serializer.errors == {}


def test_invalid_movie_serializer():
    invalid_serializer_data = dummy_movies_invalid_data.create(iterations=1)[0]
    serializer = MovieSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.data == invalid_serializer_data
    assert serializer.errors == {"year": ["This field is required."]}
