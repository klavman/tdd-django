import pytest

from .factory_movies import MovieFactory


@pytest.mark.django_db
def test_movie_model():
    movie = MovieFactory()
    movie.save()
    assert str(movie) == f"{movie.title} - {movie.year}"
