# import json

import pytest

from django.urls import reverse

from movies.models import Movie

from .dummy_endpoint import dummy_movies_valid_data, dummy_movies_invalid_data

# from .factory_movies import MovieFactory


@pytest.mark.django_db
def test_add_movie(client):
    movies = Movie.objects.all()
    assert len(movies) == 0

    valid_data = dummy_movies_valid_data.create(iterations=1)[0]

    resp = client.post("/api/movies/", valid_data, content_type="application/json")
    assert resp.status_code == 201
    assert resp.data["title"] == valid_data["title"]

    assert Movie.objects.count() == 1


@pytest.mark.django_db
def test_add_movie_invalid_json(client):
    movies = Movie.objects.all()
    assert movies.count() == 0

    resp = client.post("/api/movies/", {}, content_type="application/json")

    assert resp.status_code == 400

    movies = Movie.objects.all()
    assert movies.count() == 0


@pytest.mark.django_db
def test_add_movie_invalid_json_keys(client):
    movies = Movie.objects.all()
    assert movies.count() == 0

    invalid_data = dummy_movies_invalid_data.create(iterations=1)[0]

    resp = client.post("/api/movies/", invalid_data, content_type="application/json")

    assert resp.status_code == 400

    movies = Movie.objects.all()
    assert movies.count() == 0


@pytest.mark.django_db
def test_get_single_movie(client, add_movie):
    movie = add_movie()
    movie.save()
    url = reverse("api_single_movie", kwargs={"pk": movie.id})
    resp = client.get(url)
    assert resp.status_code == 200
    assert resp.data["title"] == movie.title


@pytest.mark.django_db
def test_get_single_movie_incorrect_id(client):
    movies = Movie.objects.all()
    assert movies.count() == 0

    url = reverse("api_single_movie", kwargs={"pk": 1})
    resp = client.get(url)
    assert resp.status_code == 404


@pytest.mark.django_db
def test_get_all_movies(client, add_movie):
    movie_one = add_movie()
    movie_two = add_movie()
    resp = client.get("/api/movies/")
    assert resp.status_code == 200
    assert resp.data[0]["title"] == movie_one.title
    assert resp.data[1]["title"] == movie_two.title


@pytest.mark.django_db
def test_remove_movie(client, add_movie):
    movie = add_movie()
    url = reverse("api_single_movie", kwargs={"pk": movie.id})
    resp = client.get(url)
    assert resp.status_code == 200
    assert resp.data["title"] == movie.title

    resp_two = client.delete(f"/api/movies/{movie.id}")
    assert resp_two.status_code == 204

    resp_three = client.get("/api/movies/")
    assert resp_three.status_code == 200
    assert len(resp_three.data) == 0


@pytest.mark.django_db
def test_remove_movie_incorrect_id(client):
    resp = client.delete("/api/movies/99")
    assert resp.status_code == 404


@pytest.mark.django_db
def test_update_movie(client, add_movie):
    movie = add_movie()
    movie.save()

    resp = client.put(
        f"/api/movies/{movie.id}",
        {"title": "The Big Lebowski", "genre": "comedy", "year": "1997"},
        content_type="application/json",
    )
    assert resp.status_code == 200
    assert resp.data["title"] == "The Big Lebowski"
    assert resp.data["year"] == "1997"

    resp_two = client.get(f"/api/movies/{movie.id}")
    assert resp_two.status_code == 200
    assert resp_two.data["title"] == "The Big Lebowski"
    assert resp.data["year"] == "1997"


@pytest.mark.django_db
def test_update_movie_incorrect_id(client):
    resp = client.put("/api/movies/99")
    assert resp.status_code == 404


# @pytest.mark.django_db
# def test_update_movie_invalid_json(client, add_movie):
#     movie = add_movie()
#     resp = client.put(f"/api/movies/{movie.id}", {}, content_type="application/json")
#     assert resp.status_code == 400


# @pytest.mark.django_db
# def test_update_movie_invalid_json_keys(client, add_movie):
#     movie = add_movie()

#     resp = client.put(
#         f"/api/movies/{movie.id}",
#         {"title": movie.title, "genre": movie.genre},
#         content_type="application/json",
#     )
#     assert resp.status_code == 400


@pytest.mark.django_db
@pytest.mark.parametrize(
    "add_movie, payload, status_code",
    [
        ["add_movie", {}, 400],
        ["add_movie", {"title": "The Big Lebowski", "genre": "comedy"}, 400],
    ],
    indirect=["add_movie"],
)
def test_update_movie_invalid_json(client, add_movie, payload, status_code):
    movie = add_movie()
    movie.save()
    resp = client.put(
        f"/api/movies/{movie.id}",
        payload,
        content_type="application/json",
    )
    assert resp.status_code == status_code
