from mimesis.schema import Field, Schema

_ = Field("es")

dummy_movies_valid_data = Schema(
    lambda: {"title": _("title"), "genre": _("word"), "year": str(_("year"))},
)

dummy_movies_invalid_data = Schema(
    lambda: {"title": _("title"), "genre": _("word")},
)
